import sqlite3
from scapy.all import *
import signal
import sys


packets = None
file = None

def parseCommandLine():
	for i in range(len(sys.argv)):
		if sys.argv[i] == '-f':
			global file
			file = sys.argv[i+1]


def keyboardHandler(signal, frame):
	global packets
	print packets
	print "keyboard interrupt"
	sys.exit(0)

signal.signal(signal.SIGINT, keyboardHandler)

def packetCapture():
	global packets
	packets = sniff(10)
	#packets.nsummary()
	print type(packets)
	wrpcap(file, packets)
	for packet in packets:
		print type(packet)

packetCapture()
#print packets