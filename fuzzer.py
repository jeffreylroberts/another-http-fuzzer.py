"""
	This is a program dealing with the fuzzing of web servers.

	It is made to break servers by passing garbage to them.
"""

import sys
import socket
import random
import os
import time

remoteAddress = None
remotePort = 0
iterations = 0
completelyRandom = False

def help():
	print "usage fuzzer.py --host host --port|-p port --host host [-i number] [-r]"
	print "\n\t--host\t\t\tThe host where the server is running"
	print "\t-p, --port\t\tThe port that the server is running on "
	print "\t-i, --iter\t\tNumber of iterations to run the fuzzer for"
	print "\t-r, --random\t\tSend completely random strings to the server"
	sys.exit(0)


def parseCommandLine():
	"""	parses command line arguments to set variables """
	remoteAddress = None
	remotePort = None
	iterations = None
	completelyRandom = None
	for i in range(len(sys.argv)):
		if(sys.argv[i] == '--host'):
			remoteAddress = str(sys.argv[i+1])
			i=i+1
		if(sys.argv[i] == '--port' or sys.argv[i] == '-p'):
			remotePort = int(sys.argv[i+1])
		if(sys.argv[i] == '-h'):
			"""TODO: add the help message"""
			help()
		if(sys.argv[i] == '-i' or sys.argv[i] == '--iter'):
			iterations = int(sys.argv[i+1])
		if(sys.argv[i] == '-r' or sys.argv[i] == '--random'):
			completelyRandom = True

	return remoteAddress,remotePort, iterations, completelyRandom

def createSocket(address, port):
	try:
		fuzzSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		fuzzSocket.connect((remoteAddress, remotePort))
	except socket.error:
		#Handle error
		sys.exit(0)
	return fuzzSocket

def generateRandomString(length = 100):
	"""
		Generates a random string which will be sent to the server that
		is specified to fuzzing.

		@param length: the length of the binary string to be generated

		@return string of length length containing random values
	"""
	return "get " + os.urandom(length)

def fuzz(iterations, completelyRandom):
	"""
		Handles sending network packets and generating new binary strings to send to the server.
	"""
	fuzzSocket = createSocket(remoteAddress, remotePort)
	packetPayload = generateRandomString(150)
	#packetPayload = sendString + str(packetPayload)
	for i in xrange(iterations):
		fuzzSocket.send(str(packetPayload))
		randlength = random.randint(1, 3500)
		packetPayload = generateRandomString(randlength)
		print i
		print randlength
		time.sleep(.1)
		#data = fuzzSocket.recv(500)
		#print data
	fuzzSocket.close()
	

remoteAddress, remotePort, iterations, completelyRandom = parseCommandLine()
fuzz(iterations, completelyRandom)
# print packetPayload